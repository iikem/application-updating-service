﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace Update
{
    class Program
    {
        public static string ExePath = "";

        public static void Update()
        {
            string[] files = Directory.GetFiles(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location));
            foreach (string a in files)
            {
                if (Path.GetExtension(a).Equals(".updt"))
                {
                    if (File.Exists(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), Path.GetFileNameWithoutExtension(a))))
                    {
                        try
                        {
                            File.Delete(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), Path.GetFileNameWithoutExtension(a)));
                            File.Move(a, Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), Path.GetFileNameWithoutExtension(a)));
                        }
                        catch (Exception)
                        {
                            return;
                        }
                        try
                        {
                            Process.Start(@Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), Path.GetFileNameWithoutExtension(a)), " -nupdt");
                            Environment.Exit(0);
                            return;
                        }
                        catch (Exception)
                        {
                            return;
                        }
                    }
                }
            }
        }

        public static void Retrive(string host,string app,string version)
        {
            WebClient wclient = new WebClient();
            try
            {
                wclient.DownloadFile(host + "/update?app=" + Uri.EscapeDataString(app) + "&version=" + Uri.EscapeDataString(version), Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), app + ".exe.updt"));
                Update();
            }
            catch(Exception)
            {

                return;
            }
        }


        static void Main(string[] args)
        {
            List<string> arguments = new List<string>();
            arguments.AddRange(Environment.GetCommandLineArgs());

            if(arguments.Contains("-caller"))
            {
                try
                {
                    ExePath = arguments[arguments.IndexOf("-caller") + 1];
                }
                catch
                {
                }
            }


            if (arguments.Contains("-delay"))
            {
                try
                {
                    System.Threading.Thread.Sleep(Convert.ToInt32(arguments[arguments.IndexOf("-delay") + 1]));
                }
                catch (Exception)
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }

            if (arguments.Contains("-get"))
            {
                try
                {
                    Retrive(arguments[arguments.IndexOf("-host") + 1], arguments[arguments.IndexOf("-app") + 1], arguments[arguments.IndexOf("-version") + 1]);
                    
                }
                catch (Exception Ex)
                {
                    Process.Start(@"" + ExePath, " -nupdt");
                    Environment.Exit(0);
                    return;
                }
            }
            Process.Start(@""+ExePath," -nupdt");
        }
    }
}
