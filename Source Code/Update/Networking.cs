﻿using System;
using System.Net;
using System.IO;
using System.Text;
using System.Reflection;

namespace Update
{
    public enum HttpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    };

    public enum HttpContentType
    {
        HTML,
        Plain,
        Json,
        Multipart,
        GMPART
    }


    public static class Networking
    {
        public class RestClient
        {
            public string EndPoint { get; set; }
            public HttpVerb Method { get; set; }
            public string ContentType { get; set; }
            public string PostData { get; set; }
            public byte[] PostBytes { get; set; }
            public RestClient()
            {
                EndPoint = "";
                Method = HttpVerb.GET;
                ContentType = "application/json";
                PostData = "";
            }
            public RestClient(string endpoint)
            {
                EndPoint = endpoint;
                Method = HttpVerb.GET;
                ContentType = "application/json";
                PostData = "";
            }
            public RestClient(string endpoint, HttpVerb method)
            {
                EndPoint = endpoint;
                Method = method;
                ContentType = "application/json";
                PostData = "";
            }
            public RestClient(string endpoint, HttpVerb method, string postData, string contentType)
            {
                EndPoint = endpoint;
                Method = method;
                ContentType = "application/json";
                PostData = postData;
            }
            public string MakeRequest()
            {
                return MakeRequest("");
            }
            public string MakeRequest(string parameters)
            {
                var request = (HttpWebRequest)WebRequest.Create(EndPoint + parameters);

                request.Method = Method.ToString();
                request.ContentLength = 0;
                request.ContentType = ContentType;

                if (!string.IsNullOrEmpty(PostData) && Method == HttpVerb.POST)
                {
                    var encoding = new UTF8Encoding();
                    var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData);
                    request.ContentLength = bytes.Length;

                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return "ERN";
                    }

                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }

                    return responseValue;
                }
            }
        }

        public static string Request(HttpVerb method, HttpContentType contentType,string Uri, string parameters, string data)
        {
            switch (contentType)
            {
                case HttpContentType.Json:
                    try
                    {
                        string endPoint = @"" + Uri;
                        var client = new RestClient(endPoint);
                        client.Method = method;
                        client.PostData = data;     //ex. "{postData: value}";

                        return client.MakeRequest("/" + parameters);
                    }
                    catch
                    {
                        return "ERN:SDT";
                    }
                case HttpContentType.Multipart:
                    try
                    {
                        var webClient = new WebClient();
                        string boundary = "------------------------" + DateTime.Now.Ticks.ToString("x");
                        webClient.Headers.Add("Content-Type", "multipart/form-data; boundary=" + boundary);
                        var fileData = webClient.Encoding.GetString(File.ReadAllBytes(data));
                        var package = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"uploadedfile\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n{3}\r\n--{0}--\r\n", boundary, Path.GetFileName(data), "", fileData);
                        var nfile = webClient.Encoding.GetBytes(package);
                        byte[] resp = webClient.UploadData(Uri + "/" + parameters, "POST", nfile);
                        return "";
                    }
                    catch
                    {
                        return "ERN:UPL";
                    }
            }
            return "";
        }
    }
}
